# Building Scalable Distributed Systems Individual Project
____

# Objective 
___
Implement an Ecommerce API Spec and implement it in such a way that it is scalable and handles concurrent high volume requests.

# Technology
___
+ MySQL database
+ Redis database - For session management
+ AWS EC2 Servers - Running Linux - Used to host my application and handle requests
+ AWS Load Balancers and Auto Scalling Policy - Handle high volume requests with high availability and scaling to meet demand
+ Node.JS / Express Web Framework  - Application logic implemented using server side Javascript 
+ NPM - Node package manager - Manage dependencies

# Note 
___

Northeastern Unviversity Seattle | Building Scalable Distributed Systems Project | Spring 2017